package com.marco.set.hashSet;

import java.util.*;

class Node<K> {
    private final int hash;
    private K key;
    private Node<K> next;

    public Node(int hash, K key, Node<K> next) {
        this.hash = hash;
        this.key = key;
        this.next = next;
    }

    public int getHash() {
        return hash;
    }

    public K getKey() {
        return key;
    }

    public Node<K> getNext() {
        return next;
    }

    public void setNext(Node<K> next) {
        this.next = next;
    }
}

public class HashSet<K> implements Set<K> {

    private static final int INITIAL_CAPACITY = 1 << 4; // 16

    private int bucketCapacity;
    private int size;
    private Node<K>[] buckets;

    public HashSet() {
        this(INITIAL_CAPACITY);
    }

    public HashSet(int capacity) {
        this.bucketCapacity = capacity;
        this.buckets = new Node[bucketCapacity];
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private int getKeyIndex(Object k) {
        int hashValue = (k == null) ? 0 : k.hashCode();
        hashValue %= buckets.length;
        return Math.abs(hashValue);
    }

    @Override
    public boolean contains(Object o) {
        int index = getKeyIndex(o);
        Node current = buckets[index];

        if (o == null) {
            while (current != null) {
                if (current.getKey() == null) {
                    return true;
                }
                current = current.getNext();
            }
        } else {
            while (current != null) {
                if (current.getKey().equals(o)) {
                    return true;
                }
                current = current.getNext();
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        class HashSetIterator<K> implements Iterator<K> {
            private int currentBucket;
            private Node currentNode;

            public HashSetIterator() {
                currentNode = null;
                currentBucket = -1;
            }

            @Override
            public boolean hasNext() {
                if (currentNode != null && currentNode.getNext() != null) { // currentNode has next
                    return true;
                }

                for (int i = currentBucket + 1; i < buckets.length; i++) { // there are still nodes
                    if (buckets[i] != null) {
                        return true;
                    }
                }
                return false; //nothing left
            }

            @Override
            public K next() {
                // if either the current or next node are null
                if (currentNode == null || currentNode.getNext() == null) {
                    currentBucket ++; //go to next bucket
                    // keep going until you find a bucket with a node
                    while (currentBucket < buckets.length &&
                            buckets[currentBucket] == null) {
                        currentBucket ++;
                    }
                    // if bucket array index still in bounds and make it the current node
                    if (currentBucket < buckets.length) {
                        currentNode = buckets[currentBucket];
                    } else { // otherwise there are no more elements
                        throw new NoSuchElementException();
                    }
                } else {
                    currentNode = currentNode.getNext();
                }
                return (K) currentNode.getKey();
            }
        }
        return new HashSetIterator();
    }

    @Override
    public Object[] toArray() {
        Iterator iter = iterator();
        K[] arrayToReturn = (K[])new Object[size];
        int i = 0;

        while (iter.hasNext()) {
            arrayToReturn[i] = (K)iter.next();
            i++;
        }
        return arrayToReturn;
    }

    @Override
    public boolean add(Object key) {
        final int index = getKeyIndex(key);
        final int keyHash = (key == null) ? 0 : key.hashCode();
        Node<K> entry = new Node<>(keyHash, (K) key, null);
        Node<K> current = buckets[index];

        if (current == null) {
            buckets[index] = entry;
            size++;
            return true;
        } else {
            do {
                if (current.getHash() == keyHash) {
                    return false;
                }
                if (current.getNext() != null) {
                    current = current.getNext();
                } else {
                    break;
                }
            } while (current != null);
        }
        current.setNext(entry);
        size++;
        return true;
    }

    @Override
    public boolean remove(Object key) {
        int index = getKeyIndex(key);

        Node<K> current = buckets[index];
        Node<K> prev = buckets[index];

        int counter = 0;
        if (current != null) {
            do {
                if (key != null) { // Compare with .equal  -- not null
                    if (key.equals(current.getKey())) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return true;
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return true;
                        }
                    }
                } else { // Compare with ==  -- key == null
                    if (key == current.getKey()) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return true;
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return true;
                        }
                    }
                }
                prev = current;
                current = current.getNext();
                counter++;
            } while (current != null);
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        for (Object a : c) {
            add(a);
        }
        return true;
    }

    @Override
    public void clear() {
        buckets = new Node[bucketCapacity];
        size = 0;
    }


    @Override
    public boolean removeAll(Collection c) { // todo: tests
        for (Object obj : c) {
            remove(obj);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection c) { // todo: all
        return false;
    }

    @Override
    public boolean containsAll(Collection c) { // todo: tests
        for (Object obj : c) {
            if (!contains(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) { // todo: all
        return new Object[0];
    }

    /*
    @Override
    public String toString() {
        Node currentEntry = null;
        StringBuffer sb = new StringBuffer();
        // loop through the array
        for (int index = 0; index < buckets.length; index++) {
            // we have an entry
            if (buckets[index] != null) {
                currentEntry = buckets[index];
                sb.append("[" + index + "]");
                if (currentEntry.getKey() != null) {
                    sb.append(" " + currentEntry.getKey().toString());
                } else {
                    sb.append(" null");
                }

                while (currentEntry.getNext() != null) {
                    currentEntry = currentEntry.getNext();
                    if (currentEntry.getKey() != null) {
                        sb.append(" -> " + currentEntry.getKey().toString());
                    } else {
                        sb.append(" -> null");
                    }
                }
                sb.append('\n');
            }
        }
        return sb.toString();
    } */
}
