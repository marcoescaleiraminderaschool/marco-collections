package com.marco.map.hashMap;


import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMap<K, V> implements Map<K, V> {

    private static final int INITIAL_CAPACITY = 16; // 1 << 4

    private MapEntry<K, V>[] buckets;
    private int bucketCapacity;
    private int size;

    public HashMap() {
        this(INITIAL_CAPACITY);
    }

    public HashMap(int capacity) {
        if (capacity != INITIAL_CAPACITY) {
            this.bucketCapacity = capacity;
        } else {
            bucketCapacity = INITIAL_CAPACITY;
        }
        this.buckets = new MapEntry[bucketCapacity];
        this.size = 0;
    }

    private int getKeyIndex(Object k) {
        int hashValue = (k == null) ? 0 : k.hashCode();
        hashValue %= buckets.length;
        return Math.abs(hashValue);
    }

    public int length() {
        return this.buckets.length;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }

    @Override
    public boolean containsValue(Object value) { // todo: all
        for (MapEntry<K,V> entry : buckets) {
            if (value.equals(entry.getValue())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        int hashKey = getKeyIndex(key);
        if (key == null) {
            for (MapEntry<K, V> current = buckets[hashKey]; current != null; current = current.getNext()) {
                if (current.getKey() == null) {
                    return current.getValue();
                }
            }
        } else {
            for (MapEntry<K, V> current = buckets[hashKey]; current != null; current = current.getNext()) {
                if (key.equals(current.getKey())) {
                    return current.getValue();
                }
            }
        }
        return null;
    }

    @Override
    public V put(Object key, Object value) {
        final int index = getKeyIndex(key);
        final int keyHash = (key == null) ? 0 : key.hashCode();
        MapEntry<K, V> entry = new MapEntry<>(keyHash, key, value, null);
        MapEntry<K, V> current = buckets[index];

        if (current == null) {
            buckets[index] = entry;
            size++;
            return null;
        } else {
            do {
                if (current.getHash() == keyHash) {
                    V toReturn = current.getValue();
                    current.setValue((V) value);
                    return toReturn;
                }
                if (current.getNext() != null) {
                    current = current.getNext();
                }
            } while (current.getNext() != null);
        }
        current.setNext(entry);
        size++;
        return current.getValue();
    }

    @Override
    public V remove(Object key) { // todo: tests
        int index = getKeyIndex(key);

        MapEntry<K, V> current = buckets[index];
        MapEntry<K, V> prev = buckets[index];
        V removed = null;
        int counter = 0;
        if (current != null) {
            do {
                if (key != null) { // Compare with .equal  -- not null
                    if (key.equals(current.getKey())) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return current.getValue();
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return current.getValue();
                        }
                    }
                } else { // Compare with ==  -- key == null
                    if (key == current.getKey()) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return current.getValue();
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return current.getValue();
                        }
                    }
                }
                prev = current;
                current = current.getNext();
                counter++;
            } while (current != null);
        }
        return removed;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if (m == null) {
            throw new NullPointerException();
        }
        for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        if (bucketCapacity != INITIAL_CAPACITY) {
            buckets = new MapEntry[bucketCapacity];
        } else {
            buckets = new MapEntry[INITIAL_CAPACITY];
        }
        size = 0;
    }

    @Override
    public Set keySet() {
        return null;
    }

    @Override
    public Collection values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

}
