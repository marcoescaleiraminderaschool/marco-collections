package com.marco.map.hashMap;

import java.util.Map;

public class MapEntry<K, V> implements Map.Entry<K,V> {
    private final int hash;
    private final K key;
    private V value;
    private MapEntry<K, V> next;

    public MapEntry(int hash, Object key, Object value, MapEntry<K, V> next) {
        this.hash = hash;
        this.key = (K)key;
        this.value = (V)value;
        this.next = next;
    }

    public int getHash() {
        return hash;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public V setValue(V value) {
        V oldValue = value;
        this.value = value;
        return oldValue;
    }

    public MapEntry<K, V> getNext() {
        return next;
    }

    public void setNext(MapEntry<K, V> next) {
        this.next = next;
    }
}
