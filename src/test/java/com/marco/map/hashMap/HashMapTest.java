package com.marco.map.hashMap;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapTest {
    HashMap<String, Integer> map;
    
    @Before
    public void setUp() {
        map = new HashMap<>();
    }

    @Test
    public void sizeAndIsEmpty() {
        assertTrue(map.isEmpty());
        assertEquals(0, map.size());
        map.put("john", 1);
        assertFalse(map.isEmpty());
        assertEquals(1, map.size());
    }

    @Test
    public void putAndGet() {
        assertNull( map.put(null, 1) );
        assertEquals( (Integer) 1, map.get(null) );
        assertEquals( (Integer) 1, map.put(null, 2) );
        assertEquals( (Integer) 2, map.get(null));

        assertNull( map.put("marco", 1) );
        assertNull( map.put("marcele", 5) ); //index 3
        // Same index and there is already a key there, so put returns the value of previous key | index 3
        assertEquals((Integer) 5, map.put("marcelu", 10) );

        assertEquals((Integer) 1, map.get("marco"));
        assertEquals((Integer) 5, map.get("marcele"));
        assertEquals((Integer) 10, map.get("marcelu"));

        assertNull(map.get("dahsid"));
    }

    @Test(expected = NullPointerException.class)
    public void putAll() {
        java.util.Map<String, Integer> newMap = null;
        map.putAll(newMap);

        newMap.put("john", 1);
        newMap.put("lewis", 2);
        map.putAll(newMap);

        assertEquals((Integer) 1, map.get("john"));
        assertEquals((Integer) 2, map.get("lewis"));
    }

    @Test
    public void containsKey() {
        map.put("john", 5);
        assertTrue(map.containsKey("john"));
        assertFalse(map.containsKey("lewis"));
    }

    @Test
    public void clear() {
        //Default Capacity
        map.put("john", 1);
        assertEquals(16, map.length());
        assertEquals(1, map.size());
        map.clear();
        assertNull(map.get("john"));
        assertEquals(16, map.length());
        assertEquals(0, map.size());

        //Different capacity
        HashMap<Integer, String> newMap = new HashMap<>(10);
        assertEquals(10, newMap.length());
        newMap.clear();
        assertEquals(10, newMap.length());
    }
}