package com.marco.set.hashSet;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class HashSetTest {
    HashSet<Integer> set;

    @Before
    public void setUp(){
        set= new HashSet<>();
    }

    @Test
    public void size_isEmpty() {
        assertTrue(set.isEmpty());
        assertEquals(0, set.size());
        set.add(1);
        assertFalse(set.isEmpty());
        assertEquals(1, set.size());
    }

    @Test
    public void add(){
        assertTrue(set.add(1));
        assertTrue(set.add(2));
        assertTrue(set.add(3));
        assertTrue(set.add(4));
        assertTrue(set.add(14));
        assertTrue(set.add(15));
        assertTrue(set.add(17));
        assertTrue(set.add(18));
        assertTrue(set.add(19));
        assertTrue(set.add(20));
        assertFalse(set.add(1));
        assertFalse(set.add(19));
    }

    @Test
    public void remove() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(16);
        set.add(17);
        set.add(18);
        set.add(19);
        set.add(20);
        set.add(21);
        set.add(null);

        assertTrue(set.remove(1));
        assertFalse(set.remove(1));
        assertTrue(set.remove(20));
        assertFalse(set.remove(20));
        assertTrue(set.remove(17));
        assertTrue(set.remove(null));
        assertTrue(set.remove(16));

        set.add(null);
        set.add(16);

        assertTrue(set.remove(null));
    }

    @Test
    public void clear() {
        set.add(1);
        set.add(2);
        assertEquals(2, set.size());
        // assert a contains here
        set.clear();
        // assert a contains here
        assertEquals(0, set.size());
    }

    @Test
    public void contains() {
        assertFalse("Should not contain anything",set.contains(0));
        set.add(1);
        assertTrue("Should contain 1", set.contains(1));
        set.add(17);
        assertTrue("Should contain 17", set.contains(17));
        set.add(null);
        assertTrue("Should contain null", set.contains(null));
        set.remove(null);
        set.add(16);
        set.add(null);
        assertTrue("Should contain null", set.contains(null));
    }

    @Test (expected = NoSuchElementException.class)
    public void iterator() {
        set.add(111);
        set.add(2);
        set.add(433);
        set.add(4);
        set.add(56);
        set.add(473);
        set.add(564);
        set.add(123);
        set.add(67);
        set.add(20);
        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            iter.next();
        }

        iter.next(); // throws exception
    }

    @Test
    public void toArray() {
        set.add(1);
        set.add(2);
        set.add(3);
        Integer[] toCompare = new Integer[]{1, 2, 3};
        assertArrayEquals(toCompare, set.toArray());
    }

    @Test
    public void addAll() {
        set.add(1);
        set.add(2);
        set.add(5);
        java.util.HashSet<Integer> toAdd = new java.util.HashSet<>();
        toAdd.add(3);
        toAdd.add(4);
        toAdd.add(null);

        assertTrue(set.addAll(toAdd));

        java.util.HashSet<Integer> toAdd2 = new java.util.HashSet<>();
        toAdd2.add(4);

        assertTrue(set.addAll(toAdd2));
    }
}