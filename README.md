☞ Collections Project ☜
=======================

> #### Sets

-   HashSet ✅
-   TreeSet ✅

> #### Lists

-   ArrayList ✅

-   LinkedList ✅

> #### Queue 

-   PriorityQueue ❓

> #### Maps

-   HashMap ✅
-   TreeMap ❓



Time complexity 
---------------

  
  | Method     | Array |  List | Linked List |  HashSet |  HashMap    | TreeSet |
  |------------|-------|-------|-------------|----------|-------------|---------|
  | Insertion  | O (1) | O (1) |   O (n)     |  O (n)   |  O (log(n)) | O()     |
  | Access     | O (1) | O (n) |   O (n)     |  O (n)   |  O ()       | O()     |
  | Deletion   | O (n) | O (n) |   O (n)     |  O (n)   |  O ()       | O()     |
  


Tests 
-----

You can execute all tests by running the command: `mvn clean test`

#### You can check tests with Jacoco

**open ** `target/site/jacoco/index.html`

Build 
-----

You can create a .jar file by running the command: `mvn clean install`


[Collections](http://cs.smu.ca/~porter/csc/341/notes/JavaCollectionsFrameworkOverview.png)
==========================================================================================
